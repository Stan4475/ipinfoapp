package com.stansoft.ipinfo.data

import com.stansoft.ipinfo.data.repository.IPRepositoryImpl
import com.stansoft.ipinfo.data.retrofit.service.IPInfoRetrofitService
import com.stansoft.ipinfo.domain.repository.IPRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class IPInfoDataModule {

    @Singleton
    @Provides
    fun ipInfoService(retrofit: Retrofit): IPInfoRetrofitService =
        retrofit.create(IPInfoRetrofitService::class.java)

    @Singleton
    @Provides
    fun ipRepository(repository: IPRepositoryImpl): IPRepository =
        repository

}