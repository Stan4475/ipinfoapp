package com.stansoft.ipinfo.domain

import com.stansoft.ipinfo.domain.model.IPInfoModel

object DomainFixtures {
    fun getIPInfo(
        ipAddress: String = "8.8.8.8",
        continent: String = "North America",
        country: String = "United States",
        region: String? = "California",
        city: String = "Mountain View",
        coordinates: Pair<Double, Double> = Pair(37.38600, -122.08380)
    ): IPInfoModel =
        IPInfoModel(ipAddress, continent, country, region, city, coordinates)
}