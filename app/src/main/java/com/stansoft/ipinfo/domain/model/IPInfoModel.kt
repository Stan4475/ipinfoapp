package com.stansoft.ipinfo.domain.model

data class IPInfoModel(
    val ipAddress: String,
    val continent: String,
    val country: String,
    val region: String?,
    val city: String,
    val coordinates: Pair<Double, Double> //latitude, longitude
)