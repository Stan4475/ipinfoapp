package com.stansoft.ipinfo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.stansoft.ipinfo.presentation.ip_info.IPInfoFragment
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, IPInfoFragment.newInstance())
                    .commitNow()
        }
    }
}