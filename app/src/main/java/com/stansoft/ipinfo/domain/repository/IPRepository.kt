package com.stansoft.ipinfo.domain.repository

import com.stansoft.ipinfo.domain.model.IPInfoModel

interface IPRepository {
    suspend fun getInfoByIP(ip: String): IPInfoModel
}