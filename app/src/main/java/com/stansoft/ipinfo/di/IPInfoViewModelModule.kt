package com.stansoft.ipinfo.di

import androidx.lifecycle.ViewModel
import com.stansoft.ipinfo.presentation.ip_info.IPInfoViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class IPInfoViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(IPInfoViewModel::class)
    internal abstract fun bindIPInfoViewModel(viewModel: IPInfoViewModel): ViewModel

}