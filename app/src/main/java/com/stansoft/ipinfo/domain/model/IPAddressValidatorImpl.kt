package com.stansoft.ipinfo.domain.model

import javax.inject.Inject

class IPAddressValidatorImpl @Inject constructor(): IPAddressValidator{
    val regex: Regex = Regex("^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\\.(?!\$)|\$)){4}\$")

    override
    fun isValidIPAddress(ip: String): Boolean {
        return regex.matches(ip)
    }
}