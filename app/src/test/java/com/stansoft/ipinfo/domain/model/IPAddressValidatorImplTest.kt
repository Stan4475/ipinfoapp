package com.stansoft.ipinfo.domain.model

import junit.framework.TestCase

class IPAddressValidatorImplTest : TestCase() {

    val ipAddressValidator = IPAddressValidatorImpl()
    val validIPs: List<String> = listOf("127.0.0.1","192.168.1.1", "192.168.1.255", "255.255.255.255", "0.0.0.0")

    val invalidIPs: List<String> = listOf("128.1","192.168.1.256", "-1.2.3.4", "1.1.1.1.", "3...3")

    fun testIsValidIPAddress() {
        validIPs.forEach{
            assertTrue(ipAddressValidator.isValidIPAddress(it))
        }

        invalidIPs.forEach{
            assertFalse(ipAddressValidator.isValidIPAddress(it))
        }
    }
}