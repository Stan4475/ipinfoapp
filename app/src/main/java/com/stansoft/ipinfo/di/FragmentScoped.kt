package com.stansoft.ipinfo.di

import javax.inject.Scope

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
@Target
annotation class FragmentScoped