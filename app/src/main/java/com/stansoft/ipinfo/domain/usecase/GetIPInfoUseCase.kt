package com.stansoft.ipinfo.domain.usecase

import com.stansoft.ipinfo.domain.model.IPInfoModel
import com.stansoft.ipinfo.domain.repository.IPRepository
import javax.inject.Inject

class GetIPInfoUseCase @Inject constructor(val ipRepository: IPRepository) {
    sealed class Result {
        data class Success(val data: IPInfoModel) : Result()
        data class Error(val e: Throwable) : Result()
    }

    suspend fun execute(ipAddress: String): Result {

        return try {
            Result.Success(ipRepository.getInfoByIP(ipAddress))
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}