package com.stansoft.ipinfo.data.retrofit.response

import com.squareup.moshi.Json

enum class StatusEnum(val value: String) {
    @field:Json(name = "error") ERROR("error"),
    @field:Json(name = "success") SUCCESS("success")
}
