package com.stansoft.ipinfo.presentation.ip_info

import androidx.annotation.StringRes
import androidx.lifecycle.viewModelScope
import com.stansoft.ipinfo.R
import com.stansoft.ipinfo.data.repository.IllegalResponseException
import com.stansoft.ipinfo.domain.model.IPAddressValidatorImpl
import com.stansoft.ipinfo.domain.model.IPInfoModel
import com.stansoft.ipinfo.domain.usecase.GetIPInfoUseCase
import com.stansoft.ipinfo.presentation.BaseAction
import com.stansoft.ipinfo.presentation.BaseViewModel
import com.stansoft.ipinfo.presentation.BaseViewState
import kotlinx.coroutines.launch
import javax.inject.Inject

class IPInfoViewModel @Inject constructor(
    private val getIPInfoUseCase: GetIPInfoUseCase,
    private val ipAddressValidator: IPAddressValidatorImpl
) : BaseViewModel<IPInfoViewModel.ViewState, IPInfoViewModel.Action>(ViewState()) {

    fun onSearchIP(ipAddress: String) {
        getIPInfo(ipAddress)
    }

    private fun getIPInfo(ipAddress: String) {
        if (!isValidIPAddress(ipAddress)) {
            sendAction(Action.IPInvalid)
            return
        }
        sendAction(Action.IPLoading)
        viewModelScope.launch {
            getIPInfoUseCase.execute(ipAddress).also { result ->
                val action = when (result) {
                    is GetIPInfoUseCase.Result.Success ->
                        Action.IPLoadSuccess(result.data)

                    is GetIPInfoUseCase.Result.Error ->
                        Action.IPLoadFailure(result.e)
                }
                sendAction(action)
            }
        }
    }

    fun isValidIPAddress(ipAddress: String): Boolean {
        return ipAddressValidator.isValidIPAddress(ipAddress)
    }

    override fun onReduceState(viewAction: Action): ViewState = when (viewAction) {
        is Action.IPLoading -> state.copy(
            isLoading = true,
            isError = false
        )
        is Action.IPLoadSuccess -> {
            val coordinates = viewAction.ipInfoModel.coordinates
            val coordinatesStr = "${coordinates.first};\n${coordinates.second}"
            state.copy(
                isLoading = false,
                isError = false,
                errorMessage = "",
                errorMessageRes = null,
                ipAddress = viewAction.ipInfoModel.ipAddress,
                continent = viewAction.ipInfoModel.continent,
                country = viewAction.ipInfoModel.country,
                region = viewAction.ipInfoModel.region ?: "",
                city = viewAction.ipInfoModel.city,
                coordinates = coordinatesStr
            )
        }
        is Action.IPLoadFailure -> {
            state.copy(
                isLoading = false,
                isError = true,
                errorMessage = viewAction.error.message ?: "",
                errorMessageRes = null,
                ipAddress = "",
                continent = "",
                country = "",
                region = "",
                city = "",
                coordinates = ""
            )
        }
        is Action.IPInvalid -> state.copy(
            isLoading = false,
            isError = true,
            errorMessage = "",
            errorMessageRes = R.string.invalid_ip
        )
    }

    data
    class ViewState(
        val isLoading: Boolean = false,
        val isError: Boolean = false,
        val errorMessage: String = "",
        val errorMessageRes: Int? = null,
        val ipAddress: String = "",
        val continent: String = "",
        val country: String = "",
        val region: String = "",
        val city: String = "",
        val coordinates: String = ""
    ) : BaseViewState

    sealed class Action : BaseAction {
        object IPLoading : Action()
        class IPLoadSuccess(val ipInfoModel: IPInfoModel) : Action()
        class IPLoadFailure(val error: Throwable) : Action()
        object IPInvalid : Action()
    }
}