package com.stansoft.ipinfo.domain.model

interface IPAddressValidator {
    fun isValidIPAddress(ip: String): Boolean
}