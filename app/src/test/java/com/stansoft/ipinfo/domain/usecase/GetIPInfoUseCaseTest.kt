package com.stansoft.ipinfo.domain.usecase

import com.stansoft.ipinfo.data.repository.IPRepositoryImpl
import com.stansoft.ipinfo.domain.DomainFixtures
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.net.UnknownHostException

@RunWith(JUnit4::class)
class GetIPInfoUseCaseTest {
    @MockK
    internal lateinit var mockIPInfoRepository: IPRepositoryImpl

    private lateinit var cut: GetIPInfoUseCase

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        cut = GetIPInfoUseCase(mockIPInfoRepository)
    }

    @Test
    fun `return ip info`() {
        // given
        val ipInfo = DomainFixtures.getIPInfo()
        coEvery { mockIPInfoRepository.getInfoByIP(any()) } returns ipInfo

        // when
        val result = runBlocking { cut.execute("") }

        // then
        result shouldBeEqualTo GetIPInfoUseCase.Result.Success(ipInfo)
    }

    @Test
    fun `return error when repository throws an exception`() {
        // given
        val exception = UnknownHostException()
        coEvery { mockIPInfoRepository.getInfoByIP(any()) } throws exception

        // when
        val result = runBlocking { cut.execute("") }

        // then
        result shouldBeEqualTo GetIPInfoUseCase.Result.Error(exception)
    }
}