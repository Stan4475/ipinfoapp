package com.stansoft.ipinfo.data.model

import com.squareup.moshi.Json
import com.stansoft.ipinfo.domain.model.IPInfoModel

data class IPInfoDataModel(
    @field:Json(name = "ipv4") val ipAddress: String,
    @field:Json(name = "continent_name") val continent: String,
    @field:Json(name = "country_name") val country: String,
    @field:Json(name = "subdivision_1_name") val subDivision1: String?,
    @field:Json(name = "subdivision_2_name") val subDivision2: String?,
    @field:Json(name = "city_name") val city: String,
    @field:Json(name = "latitude")val latitude: Double,
    @field:Json(name = "longitude")val longitude: Double
)

internal fun IPInfoDataModel.toDomainModel(): IPInfoModel {
    var region = subDivision1
    region?.let {
        subDivision2?.let {
            region = "$region, $subDivision2"
        }
    }

    return IPInfoModel(
        ipAddress = this.ipAddress,
        continent = this.continent,
        country = this.country,
        region = region,
        city = city,
        coordinates = Pair(latitude, longitude)
    )
}