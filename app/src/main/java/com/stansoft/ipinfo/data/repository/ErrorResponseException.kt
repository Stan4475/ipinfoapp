package com.stansoft.ipinfo.data.repository

import java.lang.Exception

class ErrorResponseException(message: String) : Exception(message) {

}
