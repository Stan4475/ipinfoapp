package com.stansoft.ipinfo.data.retrofit.response

data class ErrorModel (
    val code: Float,
    val message: String,
    val numberErrors: Int
){

}
