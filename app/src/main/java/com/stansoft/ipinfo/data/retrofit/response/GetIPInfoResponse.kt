package com.stansoft.ipinfo.data.retrofit.response

import com.squareup.moshi.Json
import com.stansoft.ipinfo.data.model.IPInfoDataModel
import com.stansoft.ipinfo.domain.model.IPInfoModel

data class GetIPInfoResponse(
    val data: IPInfoDataModel?,
    val status: StatusEnum,
    val errors: List<ErrorModel>?
)