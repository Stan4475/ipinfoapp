package com.stansoft.ipinfo.di

import com.stansoft.ipinfo.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {
    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            FragmentBuildersModule::class,
            IPInfoViewModelModule::class]
    )
    abstract fun contributeMainActivity(): MainActivity
}