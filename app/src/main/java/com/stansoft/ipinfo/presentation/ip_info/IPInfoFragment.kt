package com.stansoft.ipinfo.presentation.ip_info

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.stansoft.ipinfo.R
import com.stansoft.ipinfo.databinding.IpInfoFragmentBinding
import com.stansoft.ipinfo.presentation.extantion.observe
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class IPInfoFragment : DaggerFragment() {

    companion object {
        fun newInstance() = IPInfoFragment()
    }

    private lateinit var binding: IpInfoFragmentBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: IPInfoViewModel by lazy{
        ViewModelProvider(this, viewModelFactory)
            .get(IPInfoViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = IpInfoFragmentBinding.inflate(inflater)
        return  binding.root
    }

    private val stateObserver = Observer<IPInfoViewModel.ViewState> { state ->
        binding.progressBar.visibility = if (state.isLoading)  VISIBLE else INVISIBLE

        binding.ipInputLayout.isErrorEnabled = state.isError
        if (state.isError) {
            binding.ipInputLayout.error = state.errorMessage
            if (state.errorMessage.isEmpty()) {
                state.errorMessageRes?.let { binding.ipInputLayout.error = getString(it)}
            }
        }

        if (state.ipAddress.isNotEmpty())
            binding.ipInfoCard.visibility = VISIBLE

        binding.ipValue.text = state.ipAddress
        binding.continentValue.text = state.continent
        binding.countryValue.text = state.country

        if (state.region.isNotEmpty())
            binding.regionLayout.visibility = VISIBLE
        else
            binding.regionLayout.visibility = INVISIBLE

        binding.regionValue.text = state.region
        binding.cityValue.text = state.city
        binding.coordinatesValue.text = state.coordinates

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe(viewModel.stateLiveData, stateObserver)

        binding.searchIpButton.setOnClickListener {
            viewModel.onSearchIP(binding.ipEditText.text.toString())
        }
    }

}