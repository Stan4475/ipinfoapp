package com.stansoft.ipinfo.data.retrofit.service

import com.stansoft.ipinfo.data.model.IPInfoDataModel
import com.stansoft.ipinfo.data.retrofit.response.GetIPInfoResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface IPInfoRetrofitService {
    @GET("/{format}/{IP}")
    suspend fun getIpInfoAsync(
        @Path("IP") ip: String,
        @Path("format") format: String = "json"
    ): GetIPInfoResponse
}