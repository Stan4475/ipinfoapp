package com.stansoft.ipinfo.domain

import com.stansoft.ipinfo.domain.repository.IPRepository
import com.stansoft.ipinfo.domain.usecase.GetIPInfoUseCase
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class IPInfoDomainModule {

    @Singleton
    @Provides
    fun provideGetIPInfoUseCase(repository: IPRepository): GetIPInfoUseCase =
        GetIPInfoUseCase(repository)
}