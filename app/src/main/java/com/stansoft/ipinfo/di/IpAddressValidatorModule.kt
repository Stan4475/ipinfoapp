package com.stansoft.ipinfo.di

import com.stansoft.ipinfo.domain.model.IPAddressValidator
import com.stansoft.ipinfo.domain.model.IPAddressValidatorImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class IpAddressValidatorModule {
    @Singleton
    @Provides
    fun provideValidator(): IPAddressValidator =
        IPAddressValidatorImpl()
}