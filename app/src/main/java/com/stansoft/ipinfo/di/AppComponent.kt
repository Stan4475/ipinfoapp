package com.stansoft.ipinfo.di

import android.app.Application
import com.stansoft.ipinfo.IPInfoApplication
import com.stansoft.ipinfo.data.IPInfoDataModule
import com.stansoft.ipinfo.domain.IPInfoDomainModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityBuildersModule::class,
    AppModule::class,
    ViewModelFactoryModule::class,
    RetrofitModule::class,
    IPInfoDataModule::class,
    IPInfoDomainModule::class,
    IpAddressValidatorModule::class
])
interface AppComponent : AndroidInjector<IPInfoApplication> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): AppComponent
    }
}