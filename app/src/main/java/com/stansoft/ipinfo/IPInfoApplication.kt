package com.stansoft.ipinfo

import com.stansoft.ipinfo.di.AppComponent
import com.stansoft.ipinfo.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication


class IPInfoApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication?>? {
        return DaggerAppComponent.factory().create(this)
    }
}