package com.stansoft.ipinfo.di

import com.stansoft.ipinfo.presentation.ip_info.IPInfoFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeIPInfoFragment(): IPInfoFragment
}