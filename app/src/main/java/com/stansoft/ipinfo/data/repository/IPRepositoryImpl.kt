package com.stansoft.ipinfo.data.repository

import com.stansoft.ipinfo.data.model.toDomainModel
import com.stansoft.ipinfo.data.retrofit.response.StatusEnum
import com.stansoft.ipinfo.data.retrofit.service.IPInfoRetrofitService
import com.stansoft.ipinfo.domain.model.IPInfoModel
import com.stansoft.ipinfo.domain.repository.IPRepository
import javax.inject.Inject

class IPRepositoryImpl @Inject constructor(
    private val ipInfoService: IPInfoRetrofitService
): IPRepository {
    override suspend fun getInfoByIP(ip: String): IPInfoModel {
        val result = ipInfoService.getIpInfoAsync(ip)
        when (result.status) {
            StatusEnum.SUCCESS -> {
                if (result.data != null)
                    return result.data.toDomainModel()
                else
                    throw IllegalResponseException()
            }
            else -> {
                if (result.errors?.isNotEmpty() == true) {
                    throw ErrorResponseException(result.errors[0].message)
                }
                else
                    throw IllegalResponseException()
            }

        }
    }
}